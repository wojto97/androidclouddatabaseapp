package com.wojtekgrodecki.clouddatabaseapp.livedata

import com.wojtekgrodecki.clouddatabaseapp.navigation.NavigationEvent

class NavigationLiveEvent : LiveEvent<NavigationEvent>()