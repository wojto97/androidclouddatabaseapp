package com.wojtekgrodecki.clouddatabaseapp.communication.cloud

import com.google.firebase.firestore.FirebaseFirestore.getInstance
import com.wojtekgrodecki.clouddatabaseapp.communication.model.InfectionModelCDB

class AndroidCloudDatabase : CloudDatabase {
    private val database = getInstance()


    override fun addDailyData(
        documentName: String, infectionModel: InfectionModelCDB, onDatabaseError: () -> Unit
    ) {
        database.collection(CloudDatabaseCollections.DATA.collection)
            .document(documentName)
            .set(infectionModel)
            .addOnFailureListener { exception ->
                onDatabaseError.invoke()

            }
    }

    override fun getDataList(
        onDataFound: (foundData: String) -> Unit,
        onDatabaseError: () -> Unit
    ) {
        database.collection(CloudDatabaseCollections.DATA.collection)
            .get()
            .addOnSuccessListener { documents ->
                for (document in documents) {
                    onDataFound(document.id)
                }
            }
            .addOnFailureListener {
                onDatabaseError.invoke()

            }
    }


    override fun getDailyData(
        documentName: String,
        onGotDocumentDetails: (infectionModel: InfectionModelCDB) -> Unit,
        onDatabaseError: () -> Unit
    ) {
        database.collection(CloudDatabaseCollections.DATA.collection)
            .document(documentName)
            .addSnapshotListener { snapshot, databaseException ->
                if (databaseException != null) {
                    onDatabaseError()
                    return@addSnapshotListener
                }
                if (snapshot != null && snapshot.exists()) {
                    snapshot.toObject(InfectionModelCDB::class.java)?.let {
                        onGotDocumentDetails(it)
                    }
                }

            }
    }
}
