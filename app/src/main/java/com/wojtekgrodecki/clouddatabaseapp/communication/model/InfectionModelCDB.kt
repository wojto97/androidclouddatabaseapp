package com.wojtekgrodecki.clouddatabaseapp.communication.model

import com.wojtekgrodecki.clouddatabaseapp.domain.model.InfectionModel

data class InfectionModelCDB(
    val newInfections: String = "",
    val totalInfections: String = "",
    val newDeaths: String = "",
    val totalDeaths: String = ""
) : BaseCloudDatabaseModel<InfectionModel> {
    override fun toDomainModel() = InfectionModel(
        newInfections = this.newInfections,
        totalInfections = this.totalInfections,
        newDeaths = this.newDeaths,
        totalDeaths = this.totalDeaths
    )

}
