package com.wojtekgrodecki.clouddatabaseapp.communication.model

interface BaseCloudDatabaseModel<T> {

    fun toDomainModel(): T
}
