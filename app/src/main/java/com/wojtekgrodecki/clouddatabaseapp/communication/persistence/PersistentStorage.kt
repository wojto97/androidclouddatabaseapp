package com.wojtekgrodecki.clouddatabaseapp.communication.persistence

interface PersistentStorage {

    // TODO: Remove sample methods
    fun getExampleEntry(): String

    fun saveExampleEntry(entry: String)
}
