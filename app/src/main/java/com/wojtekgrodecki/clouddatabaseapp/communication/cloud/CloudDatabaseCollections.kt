package com.wojtekgrodecki.clouddatabaseapp.communication.cloud

enum class CloudDatabaseCollections(val collection : String) {
    DATA("DATA")
}