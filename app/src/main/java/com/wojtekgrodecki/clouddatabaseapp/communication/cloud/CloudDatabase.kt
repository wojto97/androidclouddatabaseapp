package com.wojtekgrodecki.clouddatabaseapp.communication.cloud

import com.wojtekgrodecki.clouddatabaseapp.communication.model.InfectionModelCDB

interface CloudDatabase {
    fun addDailyData(
        documentName: String, infectionModel: InfectionModelCDB, onDatabaseError: () -> Unit
    )

    fun getDataList(
        onDataFound: (foundData: String) -> Unit, onDatabaseError: () -> Unit
    )

    fun getDailyData(
        documentName: String,
        onGotDocumentDetails: (infectionModel: InfectionModelCDB) -> Unit,
        onDatabaseError: () -> Unit

    )
}