package com.wojtekgrodecki.clouddatabaseapp.common

interface DialogManager {

    fun showExitDialog(onExitConfirmed: () -> Unit)

}