package com.wojtekgrodecki.clouddatabaseapp.common

import android.content.Context
import android.content.res.loader.ResourcesProvider
import com.afollestad.materialdialogs.MaterialDialog
import com.wojtekgrodecki.clouddatabaseapp.R

class AndroidDialogManager(
    private val context: Context,
    private val resourcesProvider: ResourcesProvider
) : DialogManager {


    override fun showExitDialog(onExitConfirmed: () -> Unit) {
        MaterialDialog(context).show {
            title(R.string.tittle_dialog_exit_question)
            message(R.string.message_exit_dialog_question)
            positiveButton(R.string.positive_button_dialog_question) { onExitConfirmed() }
            negativeButton(R.string.negative_button_exit_dialog_question)
        }
    }
}
