package com.wojtekgrodecki.clouddatabaseapp.ui.fragment.documentdetail

import androidx.lifecycle.MutableLiveData
import com.wojtekgrodecki.clouddatabaseapp.R
import com.wojtekgrodecki.clouddatabaseapp.base.BaseViewModel
import com.wojtekgrodecki.clouddatabaseapp.domain.model.InfectionModel
import com.wojtekgrodecki.clouddatabaseapp.domain.service.CloudDatabaseDocumentName
import com.wojtekgrodecki.clouddatabaseapp.domain.service.CloudDatabaseService
import com.wojtekgrodecki.clouddatabaseapp.livedata.MutableLiveEvent
import com.wojtekgrodecki.clouddatabaseapp.resources.ResourcesProvider

class DocumentDetailViewModel(
    private val resourcesProvider: ResourcesProvider,
    private val cloudDatabaseDocumentName: CloudDatabaseDocumentName,
    private val cloudDatabaseService: CloudDatabaseService
) : BaseViewModel() {
    val toastEvent: MutableLiveEvent<String> = MutableLiveEvent()
    val newInfection: MutableLiveData<String> = MutableLiveData()
    val totalInfection: MutableLiveData<String> = MutableLiveData()
    val newDeaths: MutableLiveData<String> = MutableLiveData()
    val totalDeaths: MutableLiveData<String> = MutableLiveData()


    fun getDocumentDetails() {
        cloudDatabaseService.getDocumentDetails(
            cloudDatabaseDocumentName.getCurrentDocumentName(),
            onGotDocumentDetails = this::onGotDetails,
            onDatabaseError = this::showErrorMessage
        )

    }

    private fun showErrorMessage() {
        toastEvent.postValue(resourcesProvider.getString(R.string.database_error))
    }

    private fun onGotDetails(infectionModel: InfectionModel) {
        newInfection.value = infectionModel.newInfections
        totalInfection.value = infectionModel.totalInfections
        newDeaths.value = infectionModel.newDeaths
        totalDeaths.value = infectionModel.totalInfections
    }
}