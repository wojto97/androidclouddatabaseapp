package com.wojtekgrodecki.clouddatabaseapp.ui.fragment.documentlist.recycler

import androidx.recyclerview.widget.DiffUtil

class DocumentListItemDiffUtil(
    private val oldList: List<DocumentListItemViewModel>,
    private val newList: List<DocumentListItemViewModel>
) : DiffUtil.Callback() {
    override fun areItemsTheSame(oldItemPosition: Int, newItemPosition: Int): Boolean {
        return oldList[oldItemPosition].documentName == newList[newItemPosition].documentName
    }

    override fun getOldListSize() = oldList.size

    override fun getNewListSize() = newList.size

    override fun areContentsTheSame(oldItemPosition: Int, newItemPosition: Int): Boolean {
        return oldList[oldItemPosition] == newList[newItemPosition]
    }
}
