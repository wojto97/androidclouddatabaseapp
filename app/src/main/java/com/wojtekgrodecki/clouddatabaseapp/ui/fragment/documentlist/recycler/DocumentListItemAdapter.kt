package com.wojtekgrodecki.clouddatabaseapp.ui.fragment.documentlist.recycler

import android.annotation.SuppressLint
import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.RecyclerView
import com.wojtekgrodecki.clouddatabaseapp.databinding.ListItemDocumentBinding

class DocumentListItemAdapter(val onDocumentClicked: (documentName: String) -> Unit) :
    RecyclerView.Adapter<DocumentListItemViewHolder>() {

    var items = listOf<DocumentListItemViewModel>()
        set(value) {
            DiffUtil.calculateDiff(
                DocumentListItemDiffUtil(
                    items,
                    value
                )
            ).also {
                field = value
            }.dispatchUpdatesTo(this)
        }


    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): DocumentListItemViewHolder =
        DocumentListItemViewHolder(
            ListItemDocumentBinding.inflate(
                LayoutInflater.from(parent.context), parent, false
            )
        )

    override fun getItemCount(): Int = items.size

    @SuppressLint("ResourceAsColor")
    override fun onBindViewHolder(holder: DocumentListItemViewHolder, position: Int) {
        val item = items[position]
        holder.bind(item)

        holder.itemView.setOnClickListener {
            onDocumentClicked(item.documentName)
        }
    }
}
