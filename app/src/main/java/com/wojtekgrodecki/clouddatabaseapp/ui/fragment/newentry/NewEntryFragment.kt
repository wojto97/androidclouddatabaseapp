package com.wojtekgrodecki.clouddatabaseapp.ui.fragment.newentry

import android.os.Bundle
import android.view.View
import com.wojtekgrodecki.clouddatabaseapp.R
import com.wojtekgrodecki.clouddatabaseapp.base.BaseFragment
import com.wojtekgrodecki.clouddatabaseapp.databinding.FragmentNewEntryBinding
import com.wojtekgrodecki.clouddatabaseapp.navigation.NavigationEvent
import com.wojtekgrodecki.clouddatabaseapp.navigation.events.DocumentListNavigationEvent
import kotlinx.android.synthetic.main.fragment_new_entry.*
import org.koin.androidx.viewmodel.ext.android.viewModel
import org.koin.core.parameter.parametersOf


class NewEntryFragment : BaseFragment<NewEntryViewModel, FragmentNewEntryBinding>() {
    private val fixMonthNumber = 1
    override val layoutResourceId: Int = R.layout.fragment_new_entry
    override val viewModel: NewEntryViewModel by viewModel { parametersOf(requireContext()) }

    override fun bindViewVariables(viewBinding: FragmentNewEntryBinding) {
        viewBinding.viewModel = viewModel
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        viewModel.setDefaultDocumentsParametrs()
        viewModel.toastEvent.observe(this::showToast)
        new_entry_data_picker.setOnDateChangedListener { _, year, monthOfYear, dayOfMonth ->
            viewModel.onDataChanged(year, monthOfYear + fixMonthNumber, dayOfMonth)
        }
    }

    override fun handleNavigationEvent(navigationEvent: NavigationEvent) = when (navigationEvent) {
        is DocumentListNavigationEvent -> handleDocumentListNavigationEvent()
        else -> Unit
    }

    private fun handleDocumentListNavigationEvent(){
        navigationController?.navigate(R.id.action_nav_new_entry_to_nav_document_list)

    }
}
