package com.wojtekgrodecki.clouddatabaseapp.ui.fragment.newentry

enum class DataSymbol(val symbol: String) {
    EMPTY_DATA_SYMBOL("null"),
    SEPARATOR(":")
}