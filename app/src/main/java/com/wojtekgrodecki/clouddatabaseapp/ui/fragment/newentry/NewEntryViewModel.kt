package com.wojtekgrodecki.clouddatabaseapp.ui.fragment.newentry

import androidx.lifecycle.MutableLiveData
import com.wojtekgrodecki.clouddatabaseapp.R
import com.wojtekgrodecki.clouddatabaseapp.base.BaseViewModel
import com.wojtekgrodecki.clouddatabaseapp.domain.model.InfectionModel
import com.wojtekgrodecki.clouddatabaseapp.domain.service.CloudDatabaseService
import com.wojtekgrodecki.clouddatabaseapp.livedata.MutableLiveEvent
import com.wojtekgrodecki.clouddatabaseapp.navigation.events.DocumentListNavigationEvent
import com.wojtekgrodecki.clouddatabaseapp.resources.ResourcesProvider

class NewEntryViewModel(
    private val resourcesProvider: ResourcesProvider,
    private val cloudDatabaseService: CloudDatabaseService
) : BaseViewModel() {
    private val infectionsData: MutableLiveData<InfectionModel> = MutableLiveData()
    val toastEvent: MutableLiveEvent<String> = MutableLiveEvent()
    val documentName: MutableLiveData<String> = MutableLiveData()

    fun setDefaultDocumentsParametrs() {
        documentName.value = DataSymbol.EMPTY_DATA_SYMBOL.symbol
        infectionsData.value = InfectionModel()
    }

    fun onDataChanged(year: Int, month: Int, day: Int) {
        documentName.value =
            year.toString() + DataSymbol.SEPARATOR.symbol + month.toString() + DataSymbol.SEPARATOR.symbol + day.toString()
    }

    fun onNewInfectionsDataChanged(newInfections: CharSequence) {
        infectionsData.value = infectionsData.value?.copy(newInfections = newInfections.toString())
    }

    fun onTotalInfectionsDataChanged(totalInfections: CharSequence) {
        infectionsData.value =
            infectionsData.value?.copy(totalInfections = totalInfections.toString())

    }

    fun onNewDeathsDataChanged(newDeaths: CharSequence) {
        infectionsData.value = infectionsData.value?.copy(newDeaths = newDeaths.toString())

    }

    fun onTotalDeathsDataChanged(totalDeaths: CharSequence) {
        infectionsData.value = infectionsData.value?.copy(totalDeaths = totalDeaths.toString())
    }

    fun addInfectionsData() {
        if (documentName.value != null && infectionsData.value != null) {
            if (documentName.value!! != DataSymbol.EMPTY_DATA_SYMBOL.symbol &&
                infectionsData.value!!.newInfections != DataSymbol.EMPTY_DATA_SYMBOL.symbol &&
                infectionsData.value!!.totalInfections != DataSymbol.EMPTY_DATA_SYMBOL.symbol &&
                infectionsData.value!!.newDeaths != DataSymbol.EMPTY_DATA_SYMBOL.symbol &&
                infectionsData.value!!.totalDeaths != DataSymbol.EMPTY_DATA_SYMBOL.symbol
            ) {
                cloudDatabaseService.addDocument(
                    documentName.value!!,
                    infectionsData.value!!,
                    onDatabaseError = this::showErrorMessage
                )
                postNavigationEventToList()
            } else {
                toastEvent.postValue(resourcesProvider.getString(R.string.add_data_error))
            }
        } else {
            toastEvent.postValue(resourcesProvider.getString(R.string.add_data_error))
        }
    }

    private fun showErrorMessage() {
        toastEvent.postValue(resourcesProvider.getString(R.string.database_error))
    }

    private fun postNavigationEventToList() {
        navigationEvent.postValue(
            DocumentListNavigationEvent()
        )
    }
}



