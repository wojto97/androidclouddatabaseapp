package com.wojtekgrodecki.clouddatabaseapp.ui.fragment.documentlist

import android.os.Bundle
import android.view.View
import androidx.recyclerview.widget.LinearLayoutManager
import com.wojtekgrodecki.clouddatabaseapp.R
import com.wojtekgrodecki.clouddatabaseapp.base.BaseFragment
import com.wojtekgrodecki.clouddatabaseapp.databinding.FragmentDocumentListBinding
import com.wojtekgrodecki.clouddatabaseapp.navigation.NavigationEvent
import com.wojtekgrodecki.clouddatabaseapp.navigation.events.DocumentDetailNavigationEvent
import com.wojtekgrodecki.clouddatabaseapp.ui.fragment.documentlist.recycler.DocumentListItemAdapter
import com.wojtekgrodecki.clouddatabaseapp.ui.fragment.documentlist.recycler.DocumentListItemViewModel
import kotlinx.android.synthetic.main.fragment_document_list.*
import org.koin.androidx.viewmodel.ext.android.viewModel
import org.koin.core.parameter.parametersOf


class DocumentListFragment : BaseFragment<DocumentListViewModel, FragmentDocumentListBinding>() {

    private val recyclerAdapter = DocumentListItemAdapter(onDocumentClicked = this::onRecyclerItemClicked)
    override val layoutResourceId: Int = R.layout.fragment_document_list
    override val viewModel: DocumentListViewModel by viewModel { parametersOf(requireContext()) }

    override fun bindViewVariables(viewBinding: FragmentDocumentListBinding) {
        viewBinding.viewModel = viewModel
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        viewModel.getDocuments()
        setupRecyclerView()
        viewModel.toastEvent.observe(this::showToast)
        viewModel.documentList.observe(this::addItemToRecycler)
    }


    override fun handleNavigationEvent(navigationEvent: NavigationEvent) = when (navigationEvent) {
        is DocumentDetailNavigationEvent -> handleDocumentDetailNavigationEvent()
        else -> Unit
    }

    private fun handleDocumentDetailNavigationEvent(){
        navigationController?.navigate(R.id.action_nav_document_list_to_documentDetailFragment)

    }

    private fun onRecyclerItemClicked(itemName: String) {
        viewModel.onDocumentSelected(itemName)
    }

    private fun setupRecyclerView() {
        data_list_recycler.layoutManager = LinearLayoutManager(requireActivity())
        data_list_recycler.adapter = recyclerAdapter
    }

    private fun addItemToRecycler(documentItem: List<DocumentListItemViewModel>) {
        recyclerAdapter.items = documentItem
    }


}

