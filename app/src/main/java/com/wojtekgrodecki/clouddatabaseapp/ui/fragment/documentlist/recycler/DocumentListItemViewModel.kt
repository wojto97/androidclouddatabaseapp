package com.wojtekgrodecki.clouddatabaseapp.ui.fragment.documentlist.recycler

import com.wojtekgrodecki.clouddatabaseapp.base.BaseViewModel

data class DocumentListItemViewModel(
    val documentName: String
) : BaseViewModel()