package com.wojtekgrodecki.clouddatabaseapp.ui.fragment.documentdetail

import android.os.Bundle
import android.view.View
import com.wojtekgrodecki.clouddatabaseapp.R
import com.wojtekgrodecki.clouddatabaseapp.base.BaseFragment
import com.wojtekgrodecki.clouddatabaseapp.databinding.FragmentDocumentDetailBinding
import org.koin.androidx.viewmodel.ext.android.viewModel
import org.koin.core.parameter.parametersOf

class DocumentDetailFragment :
    BaseFragment<DocumentDetailViewModel, FragmentDocumentDetailBinding>() {

    override val layoutResourceId: Int = R.layout.fragment_document_detail
    override val viewModel: DocumentDetailViewModel by  viewModel { parametersOf(requireContext()) }

    override fun bindViewVariables(viewBinding: FragmentDocumentDetailBinding) {
        viewBinding.viewModel = viewModel
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        viewModel.getDocumentDetails()
        viewModel.toastEvent.observe(this::showToast)
    }
}