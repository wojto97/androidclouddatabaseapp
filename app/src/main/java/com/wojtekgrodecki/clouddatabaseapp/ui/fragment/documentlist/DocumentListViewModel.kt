package com.wojtekgrodecki.clouddatabaseapp.ui.fragment.documentlist

import androidx.lifecycle.MutableLiveData
import com.wojtekgrodecki.clouddatabaseapp.R
import com.wojtekgrodecki.clouddatabaseapp.base.BaseViewModel
import com.wojtekgrodecki.clouddatabaseapp.domain.service.CloudDatabaseDocumentName
import com.wojtekgrodecki.clouddatabaseapp.domain.service.CloudDatabaseService
import com.wojtekgrodecki.clouddatabaseapp.livedata.MutableLiveEvent
import com.wojtekgrodecki.clouddatabaseapp.navigation.events.DocumentDetailNavigationEvent
import com.wojtekgrodecki.clouddatabaseapp.resources.ResourcesProvider
import com.wojtekgrodecki.clouddatabaseapp.ui.fragment.documentlist.recycler.DocumentListItemViewModel


class DocumentListViewModel(
    private val resourcesProvider: ResourcesProvider,
    private val cloudDatabaseService: CloudDatabaseService,
    private val cloudDatabaseDocumentName: CloudDatabaseDocumentName
) : BaseViewModel() {
    val toastEvent: MutableLiveEvent<String> = MutableLiveEvent()
    val documentList: MutableLiveData<List<DocumentListItemViewModel>> = MutableLiveData(listOf())

    fun getDocuments() {
        cloudDatabaseService.getDocuments(
            onDataFound = this::onDataFound, onDatabaseError = this::showErrorMessage
        )
    }

    private fun showErrorMessage(){
        toastEvent.postValue(resourcesProvider.getString(R.string.database_error))

    }

    private fun onDataFound(data: String) {
        val isDocumentAlreadyAdded = documentList.value!!.any {
            it.documentName == data
        }
        if (!isDocumentAlreadyAdded) {
            documentList.value = listOf(
                DocumentListItemViewModel(
                    documentName = data

                ), * documentList.value!!.toTypedArray()
            )
        }
    }

    fun onDocumentSelected(documentName: String) {
        cloudDatabaseDocumentName.updateCurrentDocumentName(documentName)
        postDocumentDetailNavigationEvent()
    }

    private fun postDocumentDetailNavigationEvent() {
        navigationEvent.postValue(
            DocumentDetailNavigationEvent()
        )
    }
}




