package com.wojtekgrodecki.clouddatabaseapp.ui.fragment.documentlist.recycler

import com.wojtekgrodecki.clouddatabaseapp.base.BaseViewHolder
import com.wojtekgrodecki.clouddatabaseapp.databinding.ListItemDocumentBinding

class DocumentListItemViewHolder(viewBinding: ListItemDocumentBinding) :
    BaseViewHolder<DocumentListItemViewModel, ListItemDocumentBinding>(
        viewBinding
    ) {
    override fun bind(viewModel: DocumentListItemViewModel) {
        viewBinding.viewModel = viewModel
        viewBinding.executePendingBindings()
    }
}
