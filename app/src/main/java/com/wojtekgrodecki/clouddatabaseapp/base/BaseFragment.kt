package com.wojtekgrodecki.clouddatabaseapp.base

import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.databinding.DataBindingUtil
import androidx.databinding.ViewDataBinding
import androidx.fragment.app.Fragment
import androidx.lifecycle.LiveData
import androidx.lifecycle.Observer
import androidx.navigation.NavController
import androidx.navigation.Navigation
import com.wojtekgrodecki.clouddatabaseapp.navigation.NavigationEvent

abstract class BaseFragment<viewModelType : BaseViewModel, viewDataBindingType : ViewDataBinding> :
    Fragment() {

    abstract val layoutResourceId: Int
    abstract val viewModel: viewModelType
    lateinit var viewBinding: viewDataBindingType

    var navigationController: NavController? = null

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        inflateViewWithBinding(inflater, container)
        return viewBinding.root
    }

    private fun inflateViewWithBinding(
        inflater: LayoutInflater,
        container: ViewGroup?
    ) {
        viewBinding = DataBindingUtil.inflate(inflater, layoutResourceId, container, false)
        viewBinding.lifecycleOwner = this
        bindViewVariables(viewBinding)
    }

    open fun bindViewVariables(viewBinding: viewDataBindingType) = Unit

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        observeNavigationEvents()
    }

    private fun observeNavigationEvents() {
        viewModel.navigationEvent.observe(this::handleNavigationEvent)
    }

    open fun handleNavigationEvent(navigationEvent: NavigationEvent) = Unit

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        findNavController(view)
        super.onViewCreated(view, savedInstanceState)
    }

    private fun findNavController(view: View) {
        try {
            navigationController = Navigation.findNavController(view)
        } catch (exception: Exception) {
            Log.e("BaseFragmentError","Could not find navigation controller")
        }
    }

    fun showToast(toastText: String) {
        Toast.makeText(activity, toastText, Toast.LENGTH_LONG).show()
    }

    fun <T> LiveData<out T>.observe(onEvent: (T) -> Unit) {
        observe(viewLifecycleOwner, Observer(onEvent))
    }
}