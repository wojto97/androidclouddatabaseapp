package com.wojtekgrodecki.clouddatabaseapp.base

import android.content.Context
import com.wojtekgrodecki.clouddatabaseapp.di.*
import org.koin.android.ext.koin.androidContext
import org.koin.android.ext.koin.androidLogger
import org.koin.core.KoinComponent
import org.koin.core.context.startKoin
import org.koin.core.logger.Level

abstract class BaseAppInitializer(private val context: Context) : KoinComponent {

    fun prepareLibraries() {
        prepareUniversalLibraries()
        prepareBuildVariantLibraries()
    }

    abstract fun prepareBuildVariantLibraries()

    private fun prepareUniversalLibraries() {
        prepareKoin()
    }

    private fun prepareKoin() {
        startKoin {
            androidContext(context)
            androidLogger(Level.DEBUG)
            modules(
                appModule,
                communicationModule,
                domainModule,
                viewModelModule,
                qaModule
            )
        }
    }
}