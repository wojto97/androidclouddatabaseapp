package com.wojtekgrodecki.clouddatabaseapp.base

import androidx.lifecycle.ViewModel
import com.wojtekgrodecki.clouddatabaseapp.livedata.NavigationLiveEvent

abstract class BaseViewModel : ViewModel() {

    val navigationEvent: NavigationLiveEvent = NavigationLiveEvent()
}