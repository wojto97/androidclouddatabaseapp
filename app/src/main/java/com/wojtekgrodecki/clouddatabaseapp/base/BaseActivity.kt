package com.wojtekgrodecki.clouddatabaseapp.base

import android.os.Bundle
import android.util.Log
import androidx.databinding.DataBindingUtil
import androidx.databinding.ViewDataBinding
import androidx.fragment.app.FragmentActivity
import androidx.lifecycle.LiveData
import androidx.lifecycle.Observer
import androidx.navigation.NavController
import androidx.navigation.findNavController
import com.wojtekgrodecki.clouddatabaseapp.navigation.NavigationEvent

abstract class BaseActivity<viewModelType : BaseViewModel, viewDataBindingType : ViewDataBinding> :
    FragmentActivity() {

    abstract val layoutResourceId: Int

    abstract val viewModel: viewModelType

    lateinit var viewBinding: viewDataBindingType

    open val navigationHostLayoutId: Int? = null
    var navigationController: NavController? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        inflateViewWithBinding()
        observeNavigationEvents()
    }

    private fun inflateViewWithBinding() {
        viewBinding = DataBindingUtil.setContentView(this, layoutResourceId)
        bindViewVariables(viewBinding)
    }


    open fun bindViewVariables(viewBinding: viewDataBindingType) = Unit


    private fun observeNavigationEvents() {
        viewModel.navigationEvent.observe(this::handleNavigationEvent)
    }


    open fun handleNavigationEvent(navigationEvent: NavigationEvent) = Unit

    override fun onPostCreate(savedInstanceState: Bundle?) {
        super.onPostCreate(savedInstanceState)
        findNavigationController()
    }


    private fun findNavigationController() {
        navigationHostLayoutId?.let {
            try {
                navigationController = findNavController(it)
            } catch (exception: Exception) {
                Log.e("BaseActivityError", "Could not find navigation controller")
            }
        }
    }

    fun <T> LiveData<out T>.observe(onEvent: (T) -> Unit) {
        observe(this@BaseActivity, Observer(onEvent))
    }

}