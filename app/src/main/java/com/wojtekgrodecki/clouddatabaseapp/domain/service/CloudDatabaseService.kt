package com.wojtekgrodecki.clouddatabaseapp.domain.service

import com.wojtekgrodecki.clouddatabaseapp.communication.cloud.CloudDatabase
import com.wojtekgrodecki.clouddatabaseapp.domain.model.InfectionModel

class CloudDatabaseService(private val cloudDatabase: CloudDatabase) {


    fun getDocuments(
        onDataFound: (foundData: String) -> Unit, onDatabaseError: () -> Unit
    ) {
        cloudDatabase.getDataList(onDataFound, onDatabaseError = onDatabaseError)
    }

    fun getDocumentDetails(
        documentName: String,
        onGotDocumentDetails: (infectionModel: InfectionModel) -> Unit,
        onDatabaseError: () -> Unit

    ) {
        cloudDatabase.getDailyData(
            documentName,
            onDatabaseError = onDatabaseError,
            onGotDocumentDetails = { infectionModel ->
                onGotDocumentDetails(
                    infectionModel.toDomainModel()
                )
            })
    }

    fun addDocument(
        documentName: String, infectionModel: InfectionModel, onDatabaseError: () -> Unit
    ) {
        cloudDatabase.addDailyData(
            documentName,
            infectionModel.toCommunicationModel(),
            onDatabaseError = onDatabaseError
        )
    }
}