package com.wojtekgrodecki.clouddatabaseapp.domain.model

interface BaseDomainDatabaseModel<T> {

    fun toCommunicationModel(): T
}

