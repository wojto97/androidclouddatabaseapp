package com.wojtekgrodecki.clouddatabaseapp.domain.service

class CloudDatabaseDocumentName {
    var documentName = ""

    fun updateCurrentDocumentName(documentName: String) {
        this.documentName = documentName
    }

    fun getCurrentDocumentName(): String {
        return documentName
    }
}