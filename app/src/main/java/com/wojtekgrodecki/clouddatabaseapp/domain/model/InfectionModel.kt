package com.wojtekgrodecki.clouddatabaseapp.domain.model

import com.wojtekgrodecki.clouddatabaseapp.communication.model.InfectionModelCDB

data class InfectionModel(
    val newInfections: String = "null",
    val totalInfections: String = "null",
    val newDeaths: String = "null",
    val totalDeaths: String = "null"
) : BaseDomainDatabaseModel<InfectionModelCDB> {
    override fun toCommunicationModel() = InfectionModelCDB(
        newInfections = this.newInfections,
        totalInfections = this.totalInfections,
        newDeaths = this.newDeaths,
        totalDeaths = this.totalDeaths
    )
}
