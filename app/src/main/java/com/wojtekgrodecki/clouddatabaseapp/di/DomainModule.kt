package com.wojtekgrodecki.clouddatabaseapp.di

import com.wojtekgrodecki.clouddatabaseapp.domain.service.CloudDatabaseDocumentName
import com.wojtekgrodecki.clouddatabaseapp.domain.service.CloudDatabaseService
import org.koin.dsl.module

val domainModule = module {


    single {
        CloudDatabaseService(
            cloudDatabase = get()
        )
    }

    single {
        CloudDatabaseDocumentName(
        )
    }
}