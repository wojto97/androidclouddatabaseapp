package com.wojtekgrodecki.clouddatabaseapp.di

import android.content.Context
import com.wojtekgrodecki.clouddatabaseapp.common.AndroidDialogManager
import com.wojtekgrodecki.clouddatabaseapp.common.DialogManager
import com.wojtekgrodecki.clouddatabaseapp.resources.AndroidResourcesProvider
import com.wojtekgrodecki.clouddatabaseapp.resources.ResourcesProvider
import org.koin.core.parameter.parametersOf
import org.koin.dsl.module

val appModule = module {

    factory<ResourcesProvider> { (context: Context) ->
        AndroidResourcesProvider(
            context = context
        )
    }

    factory<DialogManager> { (context: Context) ->
        AndroidDialogManager(
            context = context,
            resourcesProvider = get { parametersOf(context) }
        )
    }
}
