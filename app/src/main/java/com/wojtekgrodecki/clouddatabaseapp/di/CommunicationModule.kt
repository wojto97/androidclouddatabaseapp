package com.wojtekgrodecki.clouddatabaseapp.di


import com.wojtekgrodecki.clouddatabaseapp.communication.cloud.AndroidCloudDatabase
import com.wojtekgrodecki.clouddatabaseapp.communication.cloud.CloudDatabase
import com.wojtekgrodecki.clouddatabaseapp.communication.persistence.PersistentStorage
import com.wojtekgrodecki.clouddatabaseapp.communication.persistence.SharedPreferencesPersistentStorage
import org.koin.android.ext.koin.androidContext
import org.koin.dsl.module

val communicationModule = module {

    single<CloudDatabase> {
        AndroidCloudDatabase(

        )
    }

    single<PersistentStorage> {
        SharedPreferencesPersistentStorage(
            context = androidContext()
        )
    }
}
