package com.wojtekgrodecki.clouddatabaseapp.di

import android.content.Context
import com.wojtekgrodecki.clouddatabaseapp.ui.activity.main.MainViewModel
import com.wojtekgrodecki.clouddatabaseapp.ui.fragment.documentdetail.DocumentDetailViewModel
import com.wojtekgrodecki.clouddatabaseapp.ui.fragment.documentlist.DocumentListViewModel
import com.wojtekgrodecki.clouddatabaseapp.ui.fragment.documentlist.recycler.DocumentListItemViewModel
import com.wojtekgrodecki.clouddatabaseapp.ui.fragment.newentry.NewEntryViewModel
import org.koin.androidx.viewmodel.dsl.viewModel
import org.koin.core.parameter.parametersOf
import org.koin.dsl.module

val viewModelModule = module {

    viewModel {
        MainViewModel(

        )
    }

    viewModel { (context: Context) ->
        NewEntryViewModel(
            cloudDatabaseService = get(), resourcesProvider = get { parametersOf(context) }
        )
    }

    viewModel { (context: Context) ->
        DocumentListViewModel(
            cloudDatabaseService = get(),
            cloudDatabaseDocumentName = get(),
            resourcesProvider = get { parametersOf(context) })
    }

    viewModel { DocumentListItemViewModel(documentName = get()) }

    viewModel { (context: Context) ->
        DocumentDetailViewModel(
            cloudDatabaseDocumentName = get(),
            cloudDatabaseService = get(),
            resourcesProvider = get { parametersOf(context) }
        )
    }

}