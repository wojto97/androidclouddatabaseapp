package com.wojtekgrodecki.clouddatabaseapp

import android.app.Application
import androidx.appcompat.app.AppCompatDelegate

class CloudDatabaseApp : Application() {

    init {
        AppCompatDelegate.setCompatVectorFromResourcesEnabled(true)
    }

    override fun onCreate() {
        super.onCreate()
        CloudatabaseAppInitializer(this).prepareLibraries()
    }
}