package com.wojtekgrodecki.clouddatabaseapp

import android.content.Context
import com.wojtekgrodecki.clouddatabaseapp.base.BaseAppInitializer
import com.facebook.flipper.android.utils.FlipperUtils
import com.facebook.flipper.core.FlipperClient
import com.facebook.soloader.SoLoader
import org.koin.core.inject

class CloudatabaseAppInitializer(private val context: Context) : BaseAppInitializer(context) {

    override fun prepareBuildVariantLibraries() {
        prepareFlipper()
    }

    private fun prepareFlipper() {
        if (BuildConfig.DEBUG && FlipperUtils.shouldEnableFlipper(context)) {
            SoLoader.init(context, false)
            val flipperInstance: FlipperClient by inject()
            flipperInstance.start()
        }
    }
}